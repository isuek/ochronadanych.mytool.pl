**Zadanie 1 /3 pkt./**

Poniższy tekst w języku polskim (dla ułatwienia są to tylko małe litery alfabetu łacińskiego bez znaków interpunkcyjnych)
został zaszyfrowany prostym szyfrem podstawieniowym monoalfabetycznym.
Napisz program, który przeprowadzi (wspomoże) kryptoanalizę tekstu i pozwoli na jego odszyfrowanie. Skorzystaj ze
znajomości częstotliwości występowania poszczególnych liter w języku polskim oraz znajomości samego języka (np. postaci
spójników i przyimków). Plik tekstowy (kryptogram.txt) dostępny jest na serwerze.

`
taqcp hytp didfy hpdp rpkza uidpkfp mlpljdp daqlya fpgzcsv kya gjclpdp zup zup zuy zuy uatcp ujdp
rlpgqjlmfy mypqd l fjkzi mrjdp hjqhpgd mya l ojfy tpf opmcp uidpt qimcp uidpt ljdp msyamcv rispky
hgcamrgpmcp cjdkyagcjly zj egpd cizup lmcvmrfyzu dpta y hjrgpzp mlymkpd mcpodp fjdj izup tic c cjdkyagcp
spmc cptpzp kp hprgjkp c rgvoikpdi zj sydzcfyas lvhgjckypd gjkqad cpqcljkyd fyamfp hjspdi c hprgjkp gjoy
mya fjkqad mcalzi l kjm lvzypd rgcv mczcirfy qj dop hgcvsfkpd rgcv gigazcfy zsjfkpd zsjf y eqpkmfyat ljqfy
lvrjzcvd ca dop hjd oazcfy lras eqv ljqfa hyd c fyadyzup fyadyzu cpmlymrpd cpcegcvrpd hprgcv kp qkj zj i
dyzup hj zjm ri fisya cplyrpd qypodyf rj ovd l ljqza kp qkya ymrkv kyasyaz mcrizcfp fimp mfdjkyd mya
ejmzyjs ifdpqkya cqtpd fphadimc y qpd mimp c fyadyzup pc kp hjqdjea hpqp gjmkya kp qlp djfzya kjm tpf
upzcvf figcp kjea y fgjeidzca sp hpckjfzya p rlpgqjlmfy lyrps ogpzya rj sjlypz oyacv jozamas zjc rj zcvdyc
sya kya ckpzya tamras sabymrjbadamas lmcpf ca skpm kp dvmat ejgca gjoyd j qimca cphymv zvgjegpb kp ovzcat
mfjgca hjqhympdam rv y oymv sypdv mdizupz rlaej gvsi rv tpf qlp dprp hgcaoyaep sypdam hjtazupz qj gcvsi ov
zya rps hjglpz tpf mlaej tic y myaqas dpr izyafdj zvgjegpb kpqpd kya mdicv rv zcpgpsy qgazcpz hyafdj pky
svmdymc j hjqgjcv pda casmrp zujz dakylp kpekpdp zya l kpmca myazy rp fpgzcsp gcvs mya kpcvlp fdpqa pgamcr
kp lpmcazy rlpgqjlmfy fi qgclyjs mya flphyd kp rpfya qyzris pzagois qypoad cp fikrimc idphyd p eqcya tamr
kjoyda nagois zj ri hjzcpz fimp gpqp hgcvtqcya tic kpdjcvz edjlp rlpgqjlmfy kp fjkzahr lhpqp y cpqpta
rgiqkjmz kjlp hprgc l fjkrgpfr sabymrjbydi rps lpgikfy rpfya mrjtp hj dprpzu rvdi p rvdi eqv hgcvtqcyamc
ogpz qimca sjtp oaqa sypd hgplj rgcv gpcv cphgcpz zyaoya qj gjojrv p rv kptrlpgqmca gjcfpcv simymc mhadkyz
zj qj tjrv hprgc jrj tamr fpgzcsv ejqdj fjk spdjlpkv kp hdjrkya tp zuza si lmfjzcvz kp myjqdj p fjk kyazu
c fjhvrp irkya mfgaz sy hgcv rvs oyzcvf c hypmfi caovs sypd zcvs fjkyp zudjmrpz y lvsigit espzu l rvs
dpmfi ovs sypd eqcya kp hjhpm cjmrpz espzu oaqcya c cypgkaf jgcazup lvmjfy hjq mczcvr fgahpfi c ogjq
cvqjlmfyzu sp ovz mrgcazup hjoyrp kpmyakyas c spfi hprgc jrj kp sypga zlyazcaf zpd egiov qdiey rgcv zpda l
fpcqa c spfjlvzu cypgazcaf loyt sy rpfya rgcv ogprkpda sabymrjbyd qizuas mfjzcv fjkyp zcvmzy fpgsy hjy
hjras oyzc c hypmfi irjzcv y tic l ejrjljmzy mrjy rlpgqjlmfy qjmypqd oyaeimp hgjoita hjqmfjfjl clgjrjl
mrahp epdjhita fdimp hprgcv pc y espzu tic ejrjl kj lvegpdam hpkya oymya dazc qgiep gcazc kyz mfjkzcjkp
rgcaop mfphpz mya l rat symya p rj tamr ljqp mlyazjkp qypoad figzcv mya y fgcrimy pc cyskv hjr kp kys oyta
dazc hpk fpca mdiep simy mfphpd mya oyaqpf hj mcvta lvdazypd hjras tpf c hgjzv jrgcpmd mya qogis hpgmfkpd
gpckya ragpc ticam l kpmcat sjzv kptejgarmcps jqovd dpckya tamczca taqkj oaqcya flyrp cpgpc hafkya sjz
zcpgrjlmfp hprgcpt jrj tamr fjoyarp sjtp cjkazcfp rlpgqjlmfp tp kp gjf i oadcaoiop hgcvtsa cp zyaoya
syamcfpkya kyazu hgcac rak gjf sjtp diop c rjop tpf c sacas cjmrpkya hgcvmypc tat sydjmz mcpzikaf y
hjmdimcakmrlj oac egpkyz cdpsyamc zujz taqak lpgikaf tic zpdp iejqp cp kyz qypoad qj kyaej hjd izup hjd
jfp clgjzyd qj mpsfy kyov hprgcv kyov mdizup rvszcpmas tic odymfj fdpsfy eqv si rlpgqjlmfy qjfizcp jq
qgcly jq jfyak jqhvzup zcsvzukplmcv qcyigfp jq fdizcp qjrpq tpf zcsvzup rpf zcsvzup
`

**Zadanie 2 /3 pkt./**

Napisz program do szyfrowania tekstu szyfrem Cezara w taki sposób, żeby uwzględniał alfabet polski w układzie: „duże
litery, małe litery, cyfry” („AĄBCĆ…Żaąbcć…ż0123456789”) oraz dowolne przesunięcie (ale krótsze od długości alfabetu).
Parametrem programu ma być ciąg znaków (tekst jawny lub kryptogram), wartość przesunięcia oraz opcja decydująca o
szyfrowaniu/deszyfrowaniu.

**Zadanie 3 /3 pkt./**

Napisz program do szyfrowania tekstu szyfrem XOR.
Parametrem programu ma być ciąg znaków (tekst jawny lub kryptogram) oraz klucz szyfrowania.

**Zadanie 4 /3 pkt./**

Napisz program do szyfrowania tekstu szyfrem Vigenère'a.
Wykorzystaj kod napisany przy szyfrze Cezara. Pamiętaj o możliwości odwrócenia klucza przy operacji deszyfrowania.
Parametrem programu ma być ciąg znaków (tekst jawny lub kryptogram), klucz szyfrowania oraz opcja decydująca o
szyfrowaniu/deszyfrowaniu.

**Zadanie 5 /6 pkt./**

Napisz program do szyfrowania tekstu szyfrem Playfair.
Parametrem programu ma być ciąg znaków (tekst jawny lub kryptogram), klucz szyfrowania oraz opcja decydująca o
szyfrowaniu/deszyfrowaniu.