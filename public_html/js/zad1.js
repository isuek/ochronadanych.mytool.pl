var zad1 = (function (){
    var alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
    var spojniki = ['a', 'o', 'w', 'z', 'i', 'u']
    var kryptogram = 'taqcp hytp didfy hpdp rpkza uidpkfp mlpljdp daqlya fpgzcsv kya gjclpdp zup zup zuy zuy uatcp ujdp rlpgqjlmfy mypqd l fjkzi mrjdp hjqhpgd mya l ojfy tpf opmcp uidpt qimcp uidpt ljdp msyamcv rispky hgcamrgpmcp cjdkyagcjly zj egpd cizup lmcvmrfyzu dpta y hjrgpzp mlymkpd mcpodp fjdj izup tic c cjdkyagcp spmc cptpzp kp hprgjkp c rgvoikpdi zj sydzcfyas lvhgjckypd gjkqad cpqcljkyd fyamfp hjspdi c hprgjkp gjoy mya fjkqad mcalzi l kjm lvzypd rgcv mczcirfy qj dop hgcvsfkpd rgcv gigazcfy zsjfkpd zsjf y eqpkmfyat ljqfy lvrjzcvd ca dop hjd oazcfy lras eqv ljqfa hyd c fyadyzup fyadyzu cpmlymrpd cpcegcvrpd hprgcv kp qkj zj i dyzup hj zjm ri fisya cplyrpd qypodyf rj ovd l ljqza kp qkya ymrkv kyasyaz mcrizcfp fimp mfdjkyd mya ejmzyjs ifdpqkya cqtpd fphadimc y qpd mimp c fyadyzup pc kp hjqdjea hpqp gjmkya kp qlp djfzya kjm tpf upzcvf figcp kjea y fgjeidzca sp hpckjfzya p rlpgqjlmfy lyrps ogpzya rj sjlypz oyacv jozamas zjc rj zcvdyc sya kya ckpzya tamras sabymrjbadamas lmcpf ca skpm kp dvmat ejgca gjoyd j qimca cphymv zvgjegpb kp ovzcat mfjgca hjqhympdam rv y oymv sypdv mdizupz rlaej gvsi rv tpf qlp dprp hgcaoyaep sypdam hjtazupz qj gcvsi ov zya rps hjglpz tpf mlaej tic y myaqas dpr izyafdj zvgjegpb kpqpd kya mdicv rv zcpgpsy qgazcpz hyafdj pky svmdymc j hjqgjcv pda casmrp zujz dakylp kpekpdp zya l kpmca myazy rp fpgzcsp gcvs mya kpcvlp fdpqa pgamcr kp lpmcazy rlpgqjlmfy fi qgclyjs mya flphyd kp rpfya qyzris pzagois qypoad cp fikrimc idphyd p eqcya tamr kjoyda nagois zj ri hjzcpz fimp gpqp hgcvtqcya tic kpdjcvz edjlp rlpgqjlmfy kp fjkzahr lhpqp y cpqpta rgiqkjmz kjlp hprgc l fjkrgpfr sabymrjbydi rps lpgikfy rpfya mrjtp hj dprpzu rvdi p rvdi eqv hgcvtqcyamc ogpz qimca sjtp oaqa sypd hgplj rgcv gpcv cphgcpz zyaoya qj gjojrv p rv kptrlpgqmca gjcfpcv simymc mhadkyz zj qj tjrv hprgc jrj tamr fpgzcsv ejqdj fjk spdjlpkv kp hdjrkya tp zuza si lmfjzcvz kp myjqdj p fjk kyazu c fjhvrp irkya mfgaz sy hgcv rvs oyzcvf c hypmfi caovs sypd zcvs fjkyp zudjmrpz y lvsigit espzu l rvs dpmfi ovs sypd eqcya kp hjhpm cjmrpz espzu oaqcya c cypgkaf jgcazup lvmjfy hjq mczcvr fgahpfi c ogjq cvqjlmfyzu sp ovz mrgcazup hjoyrp kpmyakyas c spfi hprgc jrj kp sypga zlyazcaf zpd egiov qdiey rgcv zpda l fpcqa c spfjlvzu cypgazcaf loyt sy rpfya rgcv ogprkpda sabymrjbyd qizuas mfjzcv fjkyp zcvmzy fpgsy hjy hjras oyzc c hypmfi irjzcv y tic l ejrjljmzy mrjy rlpgqjlmfy qjmypqd oyaeimp hgjoita hjqmfjfjl clgjrjl mrahp epdjhita fdimp hprgcv pc y espzu tic ejrjl kj lvegpdam hpkya oymya dazc qgiep gcazc kyz mfjkzcjkp rgcaop mfphpz mya l rat symya p rj tamr ljqp mlyazjkp qypoad figzcv mya y fgcrimy pc cyskv hjr kp kys oyta dazc hpk fpca mdiep simy mfphpd mya oyaqpf hj mcvta lvdazypd hjras tpf c hgjzv jrgcpmd mya qogis hpgmfkpd gpckya ragpc ticam l kpmcat sjzv kptejgarmcps jqovd dpckya tamczca taqkj oaqcya flyrp cpgpc hafkya sjz zcpgrjlmfp hprgcpt jrj tamr fjoyarp sjtp cjkazcfp rlpgqjlmfp tp kp gjf i oadcaoiop hgcvtsa cp zyaoya syamcfpkya kyazu hgcac rak gjf sjtp diop c rjop tpf c sacas cjmrpkya hgcvmypc tat sydjmz mcpzikaf y hjmdimcakmrlj oac egpkyz cdpsyamc zujz taqak lpgikaf tic zpdp iejqp cp kyz qypoad qj kyaej hjd izup hjd jfp clgjzyd qj mpsfy kyov hprgcv kyov mdizup rvszcpmas tic odymfj fdpsfy eqv si rlpgqjlmfy qjfizcp jq qgcly jq jfyak jqhvzup zcsvzukplmcv qcyigfp jq fdizcp qjrpq tpf zcsvzup rpf zcsvzup'
    var probabilityLetter = {a: 8.91, i: 8.21, o: 7.75, e: 7.66, z: 5.64, n: 5.52};
    var probabilityLetterArray = ['a', 'i', 'o', 'e'];
    var words = null;
    var wordsForLetterCount = null;
    var kryptWordsForLetterCount = null;
    var kryptoLettersArray = {};
    var letters = {};

    var patternText = null
    var patternTextForLetterCount = null
    var patternTextPopularWord = {};

    var PATTERN_LENGTH = 4;

    // Metoda zwraca silnię z liczby
    var NumberFactorial = function (myNumber) {
        var factorialNumber = null;
        if (myNumber > 0) {
            if (myNumber < 3) {
                factorialNumber = myNumber;
            } else {
                factorialNumber = 1;
                for (var i=myNumber; i>1; i--) {
                    factorialNumber = factorialNumber * i;
                }
            }
        }
        return factorialNumber;
    }

    // Metoda zwraca permutacje znanych liter wzorca
    var getPrmutationPatterns = function (chars) {
        var processText = '';
        var charLength = 0;
        for (var char in chars) {
            processText += char;
            charLength++;
        }
        var res = {};
        var result = [];
        var i=0
        while (i < (NumberFactorial(charLength)/2)) {
            var patternIsCorrect = false;
            var count = 0;
             while (patternIsCorrect === false) {
                 var matched = processText.match(/(.+)(?=.*?\1)/g); // get duplicate chars

                 if (!res[processText] && processText.length == charLength && matched === null) {
                     var reverseText = processText.split("").reverse().join("");
                     patternIsCorrect = true;
                     res[processText] = true;
                     res[reverseText] = true;
                     i++;
                 } else {
                     var operationText = '';
                     for (var k=0 ; k < processText.length; k++) {
                         if (k+1 >= processText.length) {
                             operationText += processText[(k+1)%processText.length];
                         } else {
                             operationText += processText[k+1]
                         }
                     }
                     if (res[operationText] && charLength > 3) {
                        processText = processText.substring(1, 2) + processText.substring(0, 1) + processText.substring(2, processText.length)
                     } else {
                          processText = operationText
                     }
                 }
            }
        }
    return res;
    }


    // Metoda zwraca listę wzorców do porównania
    var getPatternsFromText = function (text) {
        // pobieranie popularnych litera w tekście
        var popularLetter = getPopularLetter(text.replace(/(\s+|,)/g, ''));
        var results = [];
        var pattern = {};
        // przypisywanie najpopularniejszej literze w tekscie najpopularniejszą literę ze wzorca tekstowego
        for (var i=0; i < PATTERN_LENGTH; i++) {
            pattern[popularLetter[i].char] = probabilityLetterArray[i];
            // patternChars.push([popularLetter[i], probabilityLetterArray[i]]);
        }
        // generowanie listy wszystkich warianów wzorca
        var patternPrmutation = getPrmutationPatterns(pattern);
        for (var key in patternPrmutation) {
            var permutPattern = {exclude: '['};
            for (var j=0; j < key.length; j++) {
                permutPattern[popularLetter[j].char] = pattern[key[j]];
                permutPattern.exclude += '^' + pattern[key[j]] + (j == key.length-1 ? ']' : '|'); // wyrażenie regularne wykluczające litery wzorca
            }
            results.push(permutPattern);
        }
        return results;
    };

    // Metoda zwraca tablice słów/znaków w rozbicu na ilość znaków
    var getWordsForLetterCount = function (words, agr) {
        var result = {};
        var wordMap = {};
        for (var i=0; i < words.length; i++) {
            var letterCount = words[i].length;
            if (typeof result[letterCount] === 'undefined') {
                result[letterCount] = agr ? {} : [];
            }
            if (agr) {
                if (typeof result[letterCount][words[i]] === 'undefined') {
                    result[letterCount][words[i]] = 1;
                } else {
                    result[letterCount][words[i]]++;
                }
            }
            else if (typeof wordMap[words[i]] === 'undefined') {
                result[letterCount].push(words[i])
                patternTextPopularWord[words[i]] = 1;
            } else {
                patternTextPopularWord[words[i]]++;
            }
        }
        return result;
    };

    var getWords = function () {
        $.ajax({
            type: "GET",
            url: "./odm.txta",
            async: false,
            success : function(res) {
                words = res.split(/(\s+|,)/);
                wordsForLetterCount = getWordsForLetterCount(words);
                // wordsForLetterCount[1] = spojniki;
            }
        });
    };

    var getPatternText = function () {
        $.ajax({
            type: "GET",
            url: "./patternText.txta",
            async: false,
            success : function(res) {
                patternText = transformText(res).split(/(\s+|,)/);

                patternTextForLetterCount = getWordsForLetterCount(patternText);
                // wordsForLetterCount[1] = spojniki;
            }
        });
    };

    var positions = {};
    var matchedWords = {};

    var decode = function (text, shiftNumber) {
        var result = '';
        for (var i=0; i < text.length; i++) {
            var letterPos = alphabet.indexOf(text[i]);
            if (letterPos > -1) {
                var xxxPos = (letterPosshiftNumber)%alphabet.length;
                // var xxxPos = (letterPos-shiftNumber)%alphabet.length;
                // if (xxxPos > alphabet.length) {
                //      result += alphabet[alphabet.length-xxxPos];
                // } else {
                if (xxxPos < 0) {
                    xxxPos = xxxPos*(-1)
                }
                     result += alphabet[xxxPos];
                // }

            } else {
                result += text[i]
            }
        }
        return result;
    }

    var replaceText = function (text, pattern) {
        log('Zamieniam na podstawie wzorca tekst publiczny na kryptogram lub odwrotnie');
        var result = '';
        for (var i=0; i < text.length; i++) {
            var letter = text[i];
                if (pattern[letter]) {
                    log(letter + ' -> ' +pattern[letter], true);
                    result += pattern[letter];
                } else {
                    log('Brak znaku "' + letter + '" we wzorcu (zostaje tan sam)', true);
                    result += text[i]
                }
        }
        return result;
    }

    var getPopularLetter = function (text) {
        var result = {};
        for (var i=0; i < text.length; i++) {
            var letter = text[i];
            // if (letter === ' ') {
            //     break;
            // }
            if (typeof result[letter] === 'undefined') {
                result[letter] = 1
            }
            result[letter]++

            var retArrat = [];
            for (var key in result) {
                retArrat.push({char: key, count: result[key]});
            }
            retArrat.sort(function(a, b){return b.count-a.count})
        }
        return retArrat;
    };

    // var getWordsByLengthFromDictionary = function (wordLength, chars, min) {
    //     var result = [];
    //     var pattern = '';
    //     if (typeof chars === 'object') {
    //         for (var i=0; i < chars.length; i++) {
    //            pattern +=  '(?=.*' + chars[i] + ')'
    //         }
    //
    //     } else {
    //         pattern = chars;
    //     }
    //
    //     if (chars) {
    //         for (var key=0; key < wordsForLetterCount[wordLength].length; key++) {
    //                 var _word = wordsForLetterCount[wordLength][key];
    //                 var matched = _word.match(new RegExp(pattern, 'g'));
    //             if (_word.toLowerCase() == _word && matched != null) {
    //                 if (min && matched.length >= min) {
    //                     result.push(_word);
    //                 }
    //                 else if (!min && matched.length >= 1) {
    //                     result.push(_word);
    //                 }
    //             }
    //
    //         }
    //     } else {
    //         return wordsForLetterCount[wordLength];
    //     }
    //
    //     return result;
    // };

    // var getWordsFromCryptogram = function (wordLength, chars, min) {
    //     var result = [];
    //     var pattern = '';
    //     if (typeof chars === 'object') {
    //         for (var i=0; i < chars.length; i++) {
    //            pattern +=  '(?=.*' + chars[i] + ')'
    //         }
    //
    //     } else {
    //         pattern = chars;
    //     }
    //
    //     if (chars) {
    //         for (var _word in kryptWordsForLetterCount[wordLength]) {
    //                 var matched = _word.match(new RegExp(pattern, 'g'))
    //             if (_word.toLowerCase() == _word && matched != null) {
    //                 if (min && matched.length >= min) {
    //                     result.push(_word);
    //                 }
    //                 else if (!min && matched.length >= 1) {
    //                     result.push(_word);
    //                 }
    //             }
    //
    //         }
    //     } else {
    //         return kryptWordsForLetterCount[wordLength];
    //     }
    //
    //     return result;
    // };

    var selectionWord = function (wordsForCryptogram, wordsFromDictionary, pattern) {
        // fillInAlphabetInputs(pattern)
        $('#publicText').val(replaceText($('#kryptogram').val(), pattern));
        result = {count: 0, filed: 0};
        for (var i=0; i < wordsForCryptogram.length; i++) {
            // iterujemy po wcześniej wybranych słowach z kryptogramu o danej długości
            var cryptoWord = wordsForCryptogram[i];
            var patterRegExpWord = '';
            var patterRegExpCount = '';
            var paternCharCount = 0;
            for (var pos = 0; pos < cryptoWord.length; pos++) {
                // iterujemy po literach w słowie
                var criptoLetter = cryptoWord[pos];
                var patternLetter = pattern[criptoLetter];

                if (patternLetter) {
                    patterRegExpWord += patternLetter;
                    patterRegExpCount += (!paternCharCount ? patternLetter : ' |'+ patternLetter);
                    paternCharCount++
                } else {
                    patterRegExpWord += pattern.exclude
                }
            }
            for (var q = 0; q < wordsFromDictionary.length; q++) {
                // iterujemy po wcześniej wybranych słowach ze słownika o danej długości
                var addWord = true;
                var dictWord = wordsFromDictionary[q].toLowerCase();
                var dictWordPaternCount = dictWord.match(new RegExp(patterRegExpCount, 'g'));
                if (typeof result[cryptoWord] === 'undefined') {
                            result[cryptoWord] = [];
                }
                if ((cryptoWord === 'zqd' && dictWord === 'tam') ||
                    (cryptoWord === 'fot' && dictWord === 'nie')){
                    debugger;
                }
                if (dictWord.match(new RegExp(patterRegExpWord, 'g')) !== null &&
                    dictWordPaternCount.length  === paternCharCount &&
                    result[cryptoWord].indexOf(dictWord) === -1) {
                        result[cryptoWord].push(dictWord)
                        result.count++
                }
            }
        }
        return result;
    };


    var getCharsFromRanking = function (compare) {
        var result = {};
            for (var cryptoChar in compare) {
            result[cryptoChar] = {char:'', count: 0};
            for (var char in compare[cryptoChar]) {
                if (result[cryptoChar].count < compare[cryptoChar][char]) {
                    result[cryptoChar].char = char
                    result[cryptoChar].count = compare[cryptoChar][char]
                }
            }
        }
        return result;
    }


    var getMatchedChars = function (wordLength, cryptoChar, char, pattern) {
        // var wordsForCryptogram = getWordsFromCryptogram(wordLength, cryptoChar, 1);

        // var wordsFromDictionary = getWordsByLengthFromDictionary(wordLength, char, 1);
        // wordsFromDictionary = selectionWord(wordsForCryptogram, wordsFromDictionary, pattern);

        var wordsForCryptogram = [];
        for (var key in kryptWordsForLetterCount[wordLength]) {
            wordsForCryptogram.push(key)
        }
        // wordsFromDictionary = selectionWord(wordsForCryptogram, wordsForLetterCount[wordLength], pattern);
        wordsFromDictionary = selectionWord(wordsForCryptogram, patternTextForLetterCount[wordLength], pattern);
        return wordsFromDictionary;
    };


    var getKryptoWordForDuplicateChar = function (posX, char) {
        var result = [];
        for (var key in kryptWordsForLetterCount[posX]) {
            var kryptoLetterPos = key.match(new RegExp(char, 'g'));
            if (kryptoLetterPos !== null && kryptoLetterPos.length > 1) {
                result.push(key);
                }
            }
        return result;
    };

    var fillInAlphabetInputs = function (alphabet, pattern) {
        log('Odświerzam dane na podstawie wzorca');
        log(pattern, true);
        var inputs = '';
        $('#alphabetInputs').empty()
        $('#alphabetInputsUpperCase').empty()
        for (var i=0; i < alphabet.length; i++) {
           inputs += '<div class="char">' +
                '<input type="text" value="' + alphabet[i] + '" class="form-control disabledInput" disabled>' +
                '<input id="crip_' + alphabet[i] + '" type="text" value="' + (pattern && pattern[alphabet[i]] ? pattern[alphabet[i]] : '') + '" class="form-control alphabetInput">' +
            '</div>';
        }
        $('#alphabetInputs').append(inputs);
    };

    var getInputsPattern = function (alphabet) {
        log('Pobieranie wzorca (litera alfabetu -> litera kodująca):');
        var result = {};
        $('.alphabetInput').each(function(key, el){
            if ($(el).val()) {
                result[alphabet[key]] = $(el).val()
            }
        });
        log(result, true);
        return result;
    };

    var transformText = function (text) {
        return text.replace(/ą/g, 'a')
                .replace(/ę/g, 'e')
                .replace(/ó/g, 'o')
                .replace(/ś/g, 's')
                .replace(/ć/g, 'c')
                .replace(/ń/g, 'n')
                .replace(/ł/g, 'l')
                .replace(/ż/g, 'z')
                .replace(/ź/g, 'z').toLowerCase();
    };

    var init = function () {
        fillInAlphabetInputs(alphabet);
        getWords();
        getPatternText();

        $('#kryptogram').val(kryptogram);
        $('#publicText').val('');

        $('#encode').unbind('click');
        $('#encode').click(function (el) {
            var text = transformText($('#publicText').val());

            $('#publicText').val(text)
            $('#kryptogram').val(replaceText(text, getInputsPattern()));
        });

        $('#decode').unbind('click');
        $('#decode').click(function (el) { var kryptWords = $('#kryptogram').val().split(' ');
            kryptWords = kryptWords.sort(function(a, b){return a.length-b.length})
            kryptWordsForLetterCount = getWordsForLetterCount(kryptWords, true);
            // for (var count=1; count < kryptWordsForLetterCount.length; count++) {
            for (var charCount in kryptWordsForLetterCount) {
                // iterujemy po długościach słów
                if (charCount >= 4) {
                    break;
                }
                var kryptoWords = kryptWordsForLetterCount[charCount];
                for (var oneKryptoWord in kryptoWords) {
                    // iterujemy po słowach z kryptogramu o danej długości
                    for (var kryptoLetterPos=0; kryptoLetterPos < oneKryptoWord.length; kryptoLetterPos++) {
                        // iterujemy po każdej literze słowa z kryptogramu
                        var kryptoLetter = oneKryptoWord[kryptoLetterPos];
                           if (typeof letters[kryptoLetter] === 'undefined') {
                               letters[kryptoLetter] = 1;
                            } else {
                                letters[kryptoLetter]++;
                            }
                        for (var k in wordsForLetterCount[charCount]) {
                            // iterowanie po słowach ze słownika z danej długości słowa
                            var word = wordsForLetterCount[charCount][k]; // słowo ze słownika o danej długości
                            var letterInPos = word[kryptoLetterPos]; // litera z danej pozycji w słowniku
                            if (typeof kryptoLettersArray[kryptoLetter] === 'undefined') {
                                kryptoLettersArray[kryptoLetter] = {};
                            }

                            if (typeof letterInPos === 'undefined') {
                                debugger;
                            }
                            if (typeof kryptoLettersArray[kryptoLetter][letterInPos] === 'undefined') {
                                kryptoLettersArray[kryptoLetter][letterInPos] = 1;
                            } else {
                                kryptoLettersArray[kryptoLetter][letterInPos]++;
                            }
                        }
                    }
                }
            }
            $('#publicText').val(replaceText($('#publicText').val(), getInputsPattern()));
        });

    };

    return {
        NumberFactorial : NumberFactorial,
        getPatternsFromText : getPatternsFromText,
        getWordsForLetterCount : getWordsForLetterCount,
        decode : decode,
        replaceText : replaceText,
        getPopularLetter : getPopularLetter,
        selectionWord : selectionWord,
        getCharsFromRanking : getCharsFromRanking,
        getMatchedChars : getMatchedChars,
        fillInAlphabetInputs : fillInAlphabetInputs,
        getInputsPattern : getInputsPattern,
        transformText : transformText,
        init : init,
    }

})();