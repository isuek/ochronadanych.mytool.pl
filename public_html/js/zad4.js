var zad4 = (function (){
    var alphabet = zad2.alphabet;
    var publicText = 'Ala ma kot';

    var getKey = function () {
        return $('#cryptoKey').val();
    };

    // Metoda zwracająca powielone hasło o długości kodowanego tekstu
    var getKeyForTextLength = function (text) {
        log('Generowanie (powielanie) hasła do długości tekstu');
        var key  = getKey();
        var patternKey = ''
        for (var i=0; i < text.length; i++) {
            if (i >= key.length ) {
                patternKey += key[i%key.length];
            } else {
                patternKey += key[i];
            }
        }
        log('Hasło: <b>' + patternKey + '</b>', true);
        return patternKey;
    };


    var getCode = function (text, codeType) {
        log('Uruchamiam algorytm kodowania');
        var codeText = '';
        var key  = getKeyForTextLength(text);

        log('Iteruję po literach tekstu');
        for (var i=0; i < text.length; i++) {
            charOfkeyPos = alphabet.indexOf(key[i]);
            charOfTextPos = alphabet.indexOf(text[i]);
            log((i+1) +'-litera tekstu <b>"' + text[i] + '"</b> ma <b>' + charOfTextPos + '</b> pozycję w alfabecie', true);
            log((i+1) +'-litera hasła <b>"' + key[i] + '"</b> ma <b>' + charOfkeyPos + '</b> pozycję w alfabecie', true);
            if (charOfTextPos > - 1 && charOfkeyPos > -1) {
                var newPos = 0;
                if (codeType == 'decode') {
                    newPos = charOfTextPos - charOfkeyPos;
                    if (newPos < 0) {
                        newPos = newPos + alphabet.length;
                        log('Jestem poniżej zakresu dłogości alfabetu. Dodaję długośc alfabetu i wchodzę w jego zakres', true);
                    }
                }
                else if (codeType == 'encode') {
                    newPos = charOfTextPos + charOfkeyPos;
                   if (newPos >= alphabet.length) {
                        log('Przekroczyłem zakres dłogości alfabetu, liczę modulo');
                        newPos = newPos % alphabet.length;
                    }
                }
                codeText += alphabet[newPos];
                log('Wygenerowany znak dla litery <b>' + text[i] + ' -> ' + alphabet[newPos] + '</b>', true);
            } else {
                log('Brak litery "' + text[i] + '" lub "' + key[i] + '" w alfabecie', true);
                codeText += text[i];
            }
        }
        return codeText
    };

    var init = function () {
        $('#kryptogram').val('');
        $('#publicText').val(publicText);

        $('#encode').unbind('click');
        $('#encode').click(function (el) {
            log('Kliknięcie w przycisk "Szyfruj"');
            var text = $('#publicText').val();
            log('Rozpoczynam kodowanie tekstu publicznego: <b>"' + text + '"</b>');
            $('#kryptogram').val(getCode(text, 'encode'));
        });

        $('#decode').unbind('click');
        $('#decode').click(function (el) {
            og('Kliknięcie w przycisk "Tłumacz"');
            var text = $('#kryptogram').val();
            log('Rozpoczynam dekodowanie kryptogramu: <b>"' + text + '"</b>');
            var code = getCode(text, 'decode')
            $('#publicText').val(code);
        });
    };
    return {
        init: init,
        alphabet: alphabet,
        getKeyForTextLength: getKeyForTextLength,
        getCode: getCode
    }

})();