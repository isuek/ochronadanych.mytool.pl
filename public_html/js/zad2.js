var zad2 = (function (){
    var alphabet = ['a','ą','b','c','ć','d','e','ę','f','g','h','i','j','k','l','ł','m','n','ń','o','ó','p','q','r','s','ś','t','u','v','w','x','y','z','ż','ź',
                    'A','Ą','B','C','Ć','D','E','Ę','F','G','H','I','J','K','L','Ł','M','N','Ń','O','Ó','P','Q','R','S','Ś','T','U','V','W','X','Y','Z','Ż','Ź',
                    '0','1','2','3','4','5','6','7','8','9'];
    var publicText = 'Ala ma kot';

    var getCaesarShift = function () {
        return parseInt($('#operKey').val());
    };

    // Metoda odświerza wyświetlany alfabet
    var refreshPattern = function () {
        var pattern = {};
        var caesarShift  = getCaesarShift();
        log('Odświerzenie wzorca o przesunięcie ' + caesarShift);
        for (var i=0; i < alphabet.length; i++) {
            var posShift = caesarShift+i;
            if (posShift >= alphabet.length ) {
                posShift = posShift%alphabet.length;
            }
            else if (posShift < 0) {
                posShift = alphabet.length+posShift;
            }
            pattern[alphabet[i]] = alphabet[posShift]
        }
        zad1.fillInAlphabetInputs(alphabet, pattern)
    };

    var init = function () {
        $('#posPlus').unbind('click');
        $('#posPlus').click(function(){
            log('Kliknięcie w przycisk "+"');
            $('#operKey').val(getCaesarShift()+1);
            refreshPattern();
        });

        $('#posMinus').unbind('click');
        $('#posMinus').click(function(){
            log('Kliknięcie w przycisk "-"');
            $('#operKey').val(getCaesarShift()-1);
            refreshPattern();
        });

        refreshPattern();
        $('#kryptogram').val('');
        $('#publicText').val(publicText);

        $('#encode').unbind('click');
        $('#encode').click(function (el) {
            log('Kliknięcie w przycisk "Szyfruj"');
            var text = $('#publicText').val();
            log('Rozpoczynam kodowanie tekstu publicznego: <b>"' + text + '"</b>');
            $('#kryptogram').val(zad1.replaceText(text, zad1.getInputsPattern(alphabet)));
        });

        $('#decode').unbind('click');
        $('#decode').click(function (el) {
            log('Kliknięcie w przycisk "Tłumacz"');
            var text = $('#kryptogram').val();
            log('Rozpoczynam dekodowanie kryptogramu: <b>"' + text + '"</b>');
            $('#publicText').val(zad1.replaceText(text, zad1.getInputsPattern(alphabet)));
        });
    };
    return {
        init: init,
        alphabet : alphabet
    }

})();