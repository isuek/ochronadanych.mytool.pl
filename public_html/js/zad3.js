var zad3 = (function (){
    var publicText = 'Ala ma kot';
    var inputKey = '';

    var getKey = function () {
        var key = parseInt($('#operKey').val());
        log('Pobrano klucz szyfrowania: <b>' + key + '</b>');
        return key;
    };

    $('#posPlus').unbind('click');
    $('#posPlus').click(function(){
        log('Kliknięcie w przycisk "+"');
        $('#operKey').val(getKey()+1);
    });

    $('#posMinus').unbind('click');
    $('#posMinus').click(function(){
        log('Kliknięcie w przycisk "-"');
        $('#operKey').val(getKey()-1);
    });

    function xor (str, key) {
        log('Wykonuję porównania XOR (klucz XOR znaki tekstu):');
        var code = ''

        for (var i = 0; i < str.length; ++i) {
            var textCharCode = str.charCodeAt(i)
            var xorCharCode = key ^ textCharCode;
            log(key + '(' + key.toString(2) + ') XOR ' + str[i] + '(' +textCharCode.toString(2) + ') = ' + xorCharCode.toString(2), true);
            code += String.fromCharCode(xorCharCode);
            log('Zakodowany znak dla litery <b>"' + str[i] + '" -> "' + String.fromCharCode(xorCharCode) + '"</b>', true);
        }
        log('Zakodowany/odkodowany tekst: <b>"' + code + '"</b>');
        return code
    }


    var init = function () {
        $('#kryptogram').val('');
        $('#publicText').val(publicText);

        $('#encode').unbind('click');
        $('#encode').click(function (el) {
            log('Kliknięcie w przycisk "Szyfruj"');
            var text = $('#publicText').val();
            log('Rozpoczynam kodowanie tekstu publicznego: <b>"' + text + '"</b>');
            $('#kryptogram').val(xor(text, getKey()));
        });

        $('#decode').unbind('click');
        $('#decode').click(function (el) {
            log('Kliknięcie w przycisk "Tłumacz"');
            var text = $('#kryptogram').val();
            log('Rozpoczynam dekodowanie kryptogramu: <b>"' + text + '"</b>');
            $('#publicText').val(xor(text, getKey()));
        });
    };

    return {
        xor : xor,
        init : init
    }

})();