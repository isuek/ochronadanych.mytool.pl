var logCount = 0;

var init = (function (){
    location.hash = location.hash ? location.hash : '#zad1';
    var scripts = {
        zad1: zad1,
        zad2: zad2,
        zad3: zad3,
        zad4: zad4,
        zad5: zad5
    };
    var hashPos = location.hash.replace('#', '');
    var setMenuPos = function (pos) {
        $('#hashId').text('.' + pos + ' {display: initial;} .not_' + pos + ' {display: none;}')
    };
    setMenuPos(hashPos);

    var executeInit = function () {
        if (scripts[hashPos] && scripts[hashPos].init) {
            scripts[hashPos].init();
        }
    };

    $(document).ready(function () {
        executeInit();
    });

    window.addEventListener("hashchange", function () {
        logCount = 0;
        hashPos = location.hash.replace('#', '');
        setMenuPos(hashPos);
        executeInit();
        $('#logSection').empty();
        logCount=0;
    }, false);
})();

$('#clearLog').click(function () {
    $('#logSection').empty();
    logCount=0;
})
var log = function (msg, notCount) {
    var style = (logCount + (notCount ? 1 : 0))%2 > 0 ? 'style="background-color: #cccccc; border-radius: 5px; padding: 8px 0 8px 5px;"' : '';
    var text = '<p ' + style + '>'
    if (!notCount) {
        logCount++;
        text += '<b>' + logCount + '.</b> '
    }
    if (typeof msg == 'string') {
        text += msg + '</p>';
    }
    else if (typeof msg == 'object') {
        text += JSON.stringify(msg) + '</p>';
        // if (msg.length) {
        //     for (var i=0; i < msg.length; i++) {
        //         text += 'pos: ' + i + ' = ' + msg[i] + '</p>';
        //     }
        // } else {
        //     for (var key in msg) {
        //         text += 'key: ' + key + ' = ' + msg[key] + '</p>';
        //     }
        // }
    }
    console.log(text);
    $('#logSection').append($(text))
}
    // $(document).ready(function () {
    //
    // });

$('#publicText').keyup(function () {
    if ($('#publicText').val().indexOf('kota') > -1 &&  $('#cat').css('display') === 'none') {
        $('#cat').show();
        var src = $('#cat').attr('src');
        $('#cat')[0].src =  src + '&' + new Date(); // refresh
        $('#publicText').css({position: "inherit", opacity: "0.8"});
    } else {
        $('#cat').hide();
    }
})